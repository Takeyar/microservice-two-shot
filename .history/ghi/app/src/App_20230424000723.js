import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="new" element={<HatsForm />} />
            <Route index element={<HatsList />} />
          </Route>
        </Routes>
        <Routes>
          <
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
