import React, { useEffect, useState } from 'react';

function HatsList(hats) {
    const [hatsList, setHatsList] = useState([])
    async function loadHats() {
        const response = await fetch("http://localhost:8090/api/hats/");
        if (response.ok) {
            const data = await response.json();
            setHatsList(data.hats)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadHats()
    }, []);

    async function deleteHat(href){
      const fetchConfig = {
          method: "delete"
      };
      const response = await fetch(`http://localhost:8090${href}`, fetchConfig);
      if(response.ok) {
          loadHats();
      }
  }

    return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufactuerer</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {hatsList.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.style_name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td>
                  <img src={ hat.picture_url } height="100" width="100"/>
                </td>
                <td>
                  <button className="btn btn-danger" onClick={() => {deleteHat(hat.href)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default HatsList
