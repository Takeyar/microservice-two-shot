import React, { useEffect, useState } from 'react';

function ShoesForm (props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.manufacturer = manufacturer;
        data.name = ;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const hatUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState ('');
    const [fabric, setFabric] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')

    const handleStyleName = (event) => {
        const value = event.target.value;
            setStyleName(value);
    }
    const handleFabric = (event) => {
        const value = event.target.value;
            setFabric(value);
    }
    const handleColor = (event) => {
        const value = event.target.value;
            setColor(value);
    }
    const handlePictureUrl = (event) => {
        const value = event.target.value;
            setPictureUrl(value);
    }
    const handleLocation = (event) => {
        const value = event.target.value;
            setLocation(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response =  await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect (() => {
        fetchData();
    }, []);

return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleStyleName} placeholder="Name" required type="text" id="style_name" name="style_name" className="form-control" value={styleName}/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabric}placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control" value={fabric}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} placeholder="Color" required type="text" id="color" name="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-mb-3">
                <textarea onChange={handlePictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" cols="30" rows="5" className="form-control" value={pictureUrl}/>
                <label htmlFor="picture_url"></label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocation} required id="location" name="location" className="form-select" value={location}>
                  <option value=''>Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                                {location.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoesForm;
