import React, { useEffect, useState } from 'react';

function ShoesList(shoes) {
    const [hatsList, setShoesList] = useState([])
    async function loadShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const data = await response.json();
            setShoesList(data.shoes)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadShoes()
    }, []);

    async function deleteShoe(href){
      const fetchConfig = {
          method: "delete"
      };
      const response = await fetch(`http://localhost:8080${href}`, fetchConfig);
      if(response.ok) {
          loadShoes();
      }
  }

    return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {ShoesList.map(shoe => {
            return (
                <tr key={shoe.href}>
                    <td>{ shoe.manufactuer }</td>
                    <td>{ shoe.name }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin }</td>
                    <td>
                        <img src={ shoe.picture_url } height="100" width="100"/>
                    </td>
                <td>
                  <button className="btn btn-danger" onClick={() => {deleteShoe(shoe.href)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ShoesList
