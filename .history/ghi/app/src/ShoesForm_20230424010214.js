import React, { useEffect, useState } from 'react';

function ShoesForm (props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    }
    const [bins, setBins] = useState([]);
    const [name, setName] = useState ('');
    const [manufacturer, setManufacturer] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [bin, setBin] = useState('')

    const handleName = (event) => {
        const value = event.target.value;
            setName(value);
    }
    const handleManufacturer = (event) => {
        const value = event.target.value;
            setManufacturer(value);
    }
    const handleColor = (event) => {
        const value = event.target.value;
            setColor(value);
    }
    const handlePictureUrl = (event) => {
        const value = event.target.value;
            setPictureUrl(value);
    }
    const handleBin = (event) => {
        const value = event.target.value;
            setBin(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response =  await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect (() => {
        fetchData();
    }, []);

return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleName} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name}/>
                <label htmlFor="style_name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturer}placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" value={manufacturer}/>
                <label htmlFor="fabric">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} placeholder="Color" required type="text" id="color" name="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-mb-3">
                <textarea onChange={handlePictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" cols="30" rows="5" className="form-control" value={pictureUrl}/>
                <label htmlFor="picture_url"></label>
              </div>
              <div className="mb-3">
                <select onChange={handleBin} required id="bin" name="bin" className="form-select" value={bin}>
                  <option value=''>Choose a location</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={id}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoesForm;
