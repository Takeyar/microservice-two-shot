import React, { useEffect, useState } from 'react';

function HatsList(hats) {
    const [hatsList, setHatsList] = useState([])
    async function loadHats() {
        const response = await fetch("http://localhost:8090/api/hats/");
        if (response.ok) {
            const data = await response.json();
            setHatsList(data.hats)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadHats()
    }, []);

    async function deleteHat(href){
      const fetchConfig = {
          method: "delete"
      };
      const response = await fetch(`http://localhost:8090${href}`, fetchConfig);
      if(response.ok) {
          loadHats();
      }
  }

    return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {shoesList.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>{ shoe.manufactuer }</td>
                <td>{ shoe.name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin }</td>
                <td>
                  <img src={ bin.picture_url } height="100" width="100"/>
                </td>
                <td>
                  <button className="btn btn-danger" onClick={() => {delete(shoe.href)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ShoesList
