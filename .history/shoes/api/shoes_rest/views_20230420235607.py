from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href",
        "bin_number",
        "bin_size"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "name"
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content ["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Number"},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoeDetailEncoder,
            safe = False,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def shoe_detail(request, pk):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            {"shoes": shoes,},
            encoder=ShoeDetailEncoder
        )
    elif request.method == "DELETE":
        
            shoes = Shoe.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse ({"message": "Nothing to see here"})
    else:
        try:
            content = json.loads(request.body)
            if "shoes" in content:
                content["shoes"] = Shoe.objects.get(id=content["shoes"])
            Shoe.objects.filter(id=pk).update(**content)
            shoes = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Nothing to see here"})
            response.status_code = 404
            return response
