from django.db import models

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    shoe_color = models.CharField(max_length=50)
