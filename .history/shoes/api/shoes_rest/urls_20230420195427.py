from django.urls import path
from .views import shoe_list,

urlpatterns = [
    path("bins/shoes/", shoe_list, name="shoe_list")
]
