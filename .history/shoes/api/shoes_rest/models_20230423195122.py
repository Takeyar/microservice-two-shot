from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveBigIntegerField(null=True)
    bin_size = models.PositiveBigIntegerField(null=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.n

    def get_api_url(self):
        return reverse("shoe_list", kwargs={"pk": self.pk})
