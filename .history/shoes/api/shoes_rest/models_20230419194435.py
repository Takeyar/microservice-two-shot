from django.db import models

class Shoes(models.Model):
    manufacturer_name = models.CharField(max_length=100)
    shoe_model = models.CharField(max_length=100)
    shoe_color = models.CharField(max_length=50)
