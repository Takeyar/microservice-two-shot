from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "id"
    ]
    encoders = {
        "bin": BinVOEncoder()
    }
    # def get_extra_data(self, o):
    #     return {"bin": o.bin.closet_name}


# class ShoeDetailEncoder(ModelEncoder):
#     model = Shoe
#     properties = [
#         "manufacturer",
#         "name",
#         "color",
#         "picture_url",
#         "bin",
#         "id"
#     ]
#     encoders = {
#         "bin": BinVOEncoder()
#     }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            # bin = get_object_or_404(BinVO, import_href=bin_href)
            print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Number"},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoeListEncoder,
            safe = False,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def shoe_detail(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=id)
        return JsonResponse(
            {"shoes": shoes,},
            encoder=ShoeListEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _= Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            shoes = Shoe.objects.get(id=id)

        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Shoe"},
                status=400

            )
        Shoe.objects.filter(id=id).updated(**content)
        shoes = Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder = ShoeDetailEncoder,
            safe = False,
        )
