from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "name"
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin":
    }
