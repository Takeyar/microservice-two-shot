from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href",
        "bin_number",
        "bin_size"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "name"
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content ["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Number"},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoeDetailEncoder,
            safe = False,
        )
@require_http_methods
def shoe_detail(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes,},
            encoder=ShoeDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin
