from django.urls import path
from .views import shoe_list, shoe_detail

urlpatterns = [
    path("bins/shoes/", shoe_list, name="shoe_list"),
    path("")
]
