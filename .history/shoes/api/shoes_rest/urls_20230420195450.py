from django.urls import path
from .views import shoe_list, shoe

urlpatterns = [
    path("bins/shoes/", shoe_list, name="shoe_list")
]
