import React, { useEffect, useState } from 'react';

function ShoesList(props) {
    const [shoes, setShoes] = useState([])
    async function loadShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadShoes()
    }, []);

    async function deleteShoe(href){
      const fetchConfig = {
          method: "delete"
      };
      const response = await fetch(`http://localhost:8080/api/shoes/${href}`, fetchConfig);
      if(response.ok) {
          loadShoes();
      }
  }

    return (
    //   {/* <table className="table table-striped">
        // <thead>
        //   <tr>
        //     <th>model name</th>
        //     <th>manufacturer</th>
        //     <th>color</th>
        //     <th>picture</th>
        //     <th>bin</th>
        //   </tr>
        // </thead>
        // <tbody>
        //   {console.log(props.data)}
        //   {shoes.map((shoe) => {
        //     return (
        //       <tr key={shoe.id}>
        //         <td>{shoe.model_name}</td>
        //         <td>{shoe.manufacturer}</td>
        //         <td>{shoe.color}</td>

        //         <td>
        //           <img src={shoe.picture_url} width={150} alt="pic img" />
        //         </td>

        //         <td>{shoe.bin}</td>
        //         <td>
        //           <button style={buttonStyle} variant="danger" onClick={() => deleteTask(shoe.id)}>Delete</button>
        //         </td>
        //       </tr>
        //     );
        //   })}
    //     </tbody>
    //   </table>
    // </> */}
    <table className="table table-striped">
        <thead>
          <tr>
            <th>manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
            {console.log(props.data)}
            {shoes.map(shoe => {
            return (
                <tr key={shoe.href}>
                    <td>{ shoe.manufactuer }</td>
                    <td>{ shoe.name }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin }</td>
                    <td>
                        <img src={ shoe.picture_url } height="100" width="100"/>
                    </td>
                <td>
                  <button className="btn btn-danger" onClick={() => {deleteShoe(shoe.href)}}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
     );
}

export default ShoesList
