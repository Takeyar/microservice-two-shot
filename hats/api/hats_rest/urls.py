from django.urls import path
from .views import hats_list, show_hats

urlpatterns = [
    path("hats/", hats_list, name="hats_create"),
    path("hats/<int:pk>/", show_hats, name="show_hats"),
]
