from django.contrib import admin
from .models import Hats, LocationVO

@admin.register(Hats)
class HatAdmin(admin.ModelAdmin):
    pass

@admin.register(LocationVO)
class LocationAdmin(admin.ModelAdmin):
    pass
