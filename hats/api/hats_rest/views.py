from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "shelf_number",
        "section_number",
        "import_href",
    ]

class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "location",
        "picture_url"

    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def hats_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            location_href = f"/api/locations/{location_vo_id}/"
            hats = Hats.objects.filter(hats=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def show_hats(request, pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder
        )
    elif request.method == "DELETE":
        try:
            hats = Hats.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            return JsonResponse ({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            if "hats" in content:
                content["hats"] = Hats.objects.get(id=content["hats"])
            Hats.objects.filter(id=pk).update(**content)
            hats = Hats.objects.get(id=pk)
            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
